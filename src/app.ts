import express, { Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/status", (request: Request, response: Response) => {
  response.status(200).send();
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

app.get("/city", (request: Request, response: Response) => {
  const city = [
    "Paris",
    "Bordeaux",
    "Lyon",
    "Strasbourg",
    "Toulouse",
    "Marseille",
  ];
  response.json(city);
});

app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(404).send("Unable to find the requested resource!");
});

 


 